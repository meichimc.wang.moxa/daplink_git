[TOC]

# DAPLink

## Overview

- DAPLink提供三種介面

  - drag-and-drop programming (拖拉燒錄code到target MCU): 完成燒錄後重新上電，如果失敗會出現`FAIL.TXT`，支援的檔案格式有bin和hex。此方法target的flash algorithm必須build在DAPLink FW中[link](https://github.com/ARMmbed/DAPLink/blob/main/docs/PORT_TARGET_FAMILY.md)。
  - serial port: 支援雙向通訊，傳送break command可以reset target MCU
  - 支援debug: 支援有CMSIS-DAP的IDE做debug
    - [pyOCD](https://github.com/mbedmicro/pyOCD)
    - [uVision](http://www.keil.com/)
    - [IAR](https://www.iar.com/)

- 小綠板 FW upgrade

  在插入USB前hold住reset button，此時小綠板進入bootloader mode，複製FW到磁碟裡再新上電，若更新失敗bootloader會出現`FAIL.TXT`

- DAPLink build output

  - `*_if.bin`: 此為base file不會被使用到
  - `*_if_crc.bin`: 燒錄在新的板子上，但是不能與舊版的BL混合使用，考慮到vector table的驗證
  - `*_if_crc.hex`: 與bin內容相同
  - `*_if_crc_legacy_0x5000.bin`: 此image被使用在已經有BL的板子，AP code從0x5000開始燒錄。此檔案是在`*_if_crc_legacy_0x8000.bin`前pending 0x3000 bytes，前0x40 bytes式複製vector table，其餘的則是填充0xFF，如此一來不需要重新build code，也可以將bin檔燒在05000的位址
  - `*_if_crc_legacy_0x8000.bin`: 此image被使用在已經有BL的板子，AP code從0x8000開始燒錄
  -  `tools/post_compute_crc.py`: 此檔案內容使用output image來產生以上檔案格式

- Image structure

  - `post_compute_crc.py` 將checksum(前7個vector會被加總 offset 0x00-0x18，值會被存放在 offset 0x1C，在特定NXP MCU中會做比對，而其他MCU則會忽略)與CRC值( base output file `*_if.bin`的CRC32值，存放在image的最後4 bytes )放入image裡

    ![](images/stm32f103xb_bl.jpg)

    ![stm32f103xb_bl_crc](images/stm32f103xb_bl_crc.jpg)

  - Vector table被定義在 `startup_MCUNAME.S` 裡

- Build information fields

  這些資訊會被放在vector table的起始位址offset 0x20，refer to ` daplink.h` 

  - `DAPLINK_BUILD_KEY`: 分辨image為bootloader(0x9B939D93)或interface(0x9B939E8F)
  - `DAPLINK_HIC_ID`: 定義使用小綠板的MCU型號，確保不會被誤燒到不同型號的MCU
  - `DAPLINK_VERSION`: 軟體版本，原本是用來檢查BL和AP軟體兼容性，現在拿掉這個功能，但可以在`details.txt`裡查看。這個改變會對一些舊版BL產生影響，因此從0x20開始的12 bytes改寫為0x00，並且重新計算checksum值。可以比對`*_if_crc.bin` 與 `*_if_crc_legacy_0x8000.bin` 
  
  ![](images/stm32f103xb_bl_info.jpg)



## Build DAPLink firmware

Run on Ubuntu 22.01.1



### Setup

- Install Python 3
- Install Git
- Install a compiler
  - [GNU Arm Embedded Toolchain](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads) . This compiler will be identified as `gcc_arm`.
  - [Arm Compiler 6](https://developer.arm.com/tools-and-software/embedded/arm-compiler) . This compiler will be identified as `armclang`. Only supported on Linux and Windows.
  - [Keil MDK](https://developer.arm.com/tools-and-software/embedded/keil-mdk) or [Arm Compiler 5](https://developer.arm.com/tools-and-software/embedded/arm-compiler/downloads/legacy-compilers#arm-compiler-5). This compiler will be identified as `armcc`. Only supported on Linux and Windows.
- Install `make` (tested with [GNU Make](https://www.gnu.org/software/make)). [CMake](https://cmake.org/) can alternatively be used in conjunction with different implementations of `make` as well as [ninja](https://ninja-build.org/).
- Install virtualenv in your global Python installation eg: `pip install virtualenv`.



Get the sources and create a virtual environment

```
git clone https://github.com/mbedmicro/DAPLink
cd DAPLink
pip install virtualenv
virtualenv --version
virtualenv venv
```

Output:

```
created virtual environment CPython3.9.13.final.0-64 in 4113ms
  creator CPython3Windows(dest=C:\Users\MeichiMC_Wang\Documents\TR\tr\IC_programmer\daplink\DAPLink\venv, clear=False, no_vcs_ignore=False, global=False)
  seeder FromAppData(download=False, pip=bundle, setuptools=bundle, wheel=bundle, via=copy, app_data_dir=C:\Users\MeichiMC_Wang\AppData\Local\pypa\virtualenv)
    added seed packages: pip==23.0.1, setuptools==67.4.0, wheel==0.38.4
  activators BashActivator,BatchActivator,FishActivator,NushellActivator,PowerShellActivator,PythonActivator
```



### Activate virtual environment (start from here)

Activate the virtual environment and update requirements. This is necessary when you open a new shell. **This should be done every time you pull new changes**

```
source venv/Scripts/activate   (For Linux)
cd venv/Scripts			(For Windows)
activate.bat   			(For Windows)
cd ../../
(venv) $ pip install -r requirements.txt //MUST TO DO
```



### Build

[DAPLink Developers Guide link](https://github.com/ARMmbed/DAPLink/blob/main/docs/DEVELOPERS-GUIDE.md)

```
python3 tools/progen_compile.py -t make_gcc_arm --clean -v stm32f103xb_bl
```

- `-t <tool>`: choose the toolchain to build. The default is `make_gcc_arm`. Other options tested are `make_gcc_arm`, `make_armclang`, `make_armcc`, `cmake_gcc_arm`, `cmake_armclang`, `cmake_armcc`.
- `--clean`: will clear existing compilation products and force recompilation of all files.
- `-v`: will make compilation process more verbose (typically listing all commands with their arguments)
- `--parallel`: enable parallel compilation within a project (projects are compiled sequentially).
- `<project>`: target project to compile (e.g. `stm32f103xb_bl`, `lpc11u35_if`), if none is specified all (140 to 150) projects will be compiled.



產生uvision projects

```
progen generate -t uvision
```



### DAPLink firmware 增加 AP size與crc判斷

**AP code size 判斷**

**Design**

![](images/stm32f103xB_memory_layout.jpg)

![](images/stm32f103xB_check_AP_size.jpg)



**Result**

:exclamation: 目前若燒錄的code大於79KB，會跳出FAIL.txt，但是code仍會被燒錄至flash (skip erase chip)。再次插拔燒錄79KB的AP，錯誤燒錄之內容會被覆蓋。

![](images/stm32f103xB_check_AP_size_result.jpg)



**Note**

預期: 產生BL虛擬磁碟後，拉AP code進去會產生fail.txt

實際: maintance沒有fail.txt，也有將AP code燒錄且跳址過去

```c
vfs_user_build_filesystem()
	// FAIL.TXT
    if (vfs_mngr_get_transfer_status() != ERROR_SUCCESS) {
        file_size = get_file_size(read_file_fail_txt);
        vfs_create_file("FAIL    TXT", read_file_fail_txt, 0, file_size);
    }

// drag-n-drop\iap_flash_intf.c
static error_t intercept_page_write(uint32_t addr, const uint8_t *buf, uint32_t size)
 	// meichi test: ERROR_AP_CODE_SIZE
    if (size > 0x00000C00) { //DAPLINK_ROM_UPDATE_SIZE
        return ERROR_AP_CODE_SIZE;
    }
```



強制在intercept_page_write fail return，以確認是否有執行到intercept_page_write function

測試結果: 有進入，且fail.txt也有正確輸出

```c
// drag-n-drop\iap_flash_intf.c
static error_t intercept_page_write(uint32_t addr, const uint8_t *buf, uint32_t size)
    // meichi test: ERROR_AP_CODE_SIZE
    if (meichi_test) { //DAPLINK_ROM_UPDATE_SIZE
        return ERROR_AP_CODE_SIZE;
    } 
```



## Hardware (stm32f103xb)

**Spec**

- Cortex-M3 72 Mhz
- 128 KB Flash
- 20 KB RAM



**Memory map**

| Region | Size   | Start       | End         |
| ------ | ------ | ----------- | ----------- |
| Flash  | 128 KB | 0x0800_0000 | 0x0802_0000 |
| SRAM   | 20 KB  | 0x2000_0000 | 0x2000_5000 |



**DAPLink default pin assignment**

| **Signal**      | **I/O** | **Symbol** | **Pin** |
| --------------- | ------- | ---------- | ------- |
| SWD / JTAG      |         |            |         |
| **SWCLK / TCK** | **O**   | **PB13**   | **26**  |
| **SWDIO / TMS** | **O**   | **PB14**   | **27**  |
| **SWDIO / TMS** | **I**   | **PB12**   | **25**  |
| SWO / TDO       | I       | PA10       | 31      |
| nRESET          | O       | PB0        | 18      |
| UART            |         |            |         |
| UART RX         | I       | PA2        | 12      |
| UART TX         | O       | PA3        | 13      |
| Button          |         |            |         |
| NF-RST But.     | I       | PB15       | 28      |
| LEDs            |         |            |         |
| Connect. LED    | O       | PB6        | 42      |
| HID LED         | O       | PA9        | 30      |
| CDC LED         | O       | PA9        | 30      |
| MSC LED         | O       | PA9        | 30      |



**Schematic**

![](images\daplink-stmf103cbt6-sch.jpg)



## Appendix

### stm32f103rbt6 開發板 10 pin connector

**![](images/stm32f103rbt6-20pin-connector.jpg)**



### stm32f103rbt6 開發板硬體接線注意事項

:exclamation:**PB12(SWDIO_IN)與PB14(SWDIO_OUT)必須對接**

**![](images/str32f103rbt6硬體注意區塊.jpg)**





# CMSIS-DAP & Openocd

## Overview

Openocd用來作為CMSIS-DAP的Commander

## CMSIS-DAP

- CMSIS-DAP (Cortex Microcontroller Software Interface Standard-Debug Access Port)是一種用來連接debug port與USB的介面韌體

  [![img](images/cmsis-dap-1.jpg)]()

- Firmware有兩個版本

  - 版本1配置使用USB HID作做為與PC的接口

  - **版本2**配置使用WinUSB作做為與PC的接口，並提供高速SWO跟蹤流 (目前使用版本)

    

## Openocd

### Introduction

- Open On-Chip Debugger，授權為GPLv2

- OpenOCD Config 將相關設定做好初始化

  - 通常這些config都是放在\tcl中

  - 語法為TCL

  - 比較常用的目錄

    - **interface**: debug adapters (cmsis-dap, jlink, stlink...)

    - board: Circuit Board, PWA, PCB

    - **target**: Chip. The “target” directory represents the **JTAG TAPs** on a chip which OpenOCD should control, not a board.

      常見的有ARM chip和FPGA/CPLD，如果一個chip上有多個TAP，則會被定義在target cfg裡。

      (目前openocd官方沒有提供IMXRT系列的cfg file，參考[Programming the FLASH memory of i.MXRT devices with OpenOCD](https://visualgdb.com/tutorials/arm/imxrt/flash/))

- Openocd是跑在host PC端，需要調試仿真器adapter (SWD/JTAG)配合使用，再使用gdb client進行調試

  ![img](images/openocd-1.jpg)

- OpenOCD與GDB之間的溝通是透過TCP/IP，GDB是一種remote serial protocol。Openocd會建立GDB server並等待GDB連接

  ![openocd-2](images/openocd-2.jpg)

  *DTM: debug transport module



### Windows setup

1. 下載[msys2](https://www.msys2.org/)

2. 開啟**MSYS2 MINGW64**，執行以下command

   ```plaintext
   # Start by updating the package database and core system packages
   pacman -Syu
   # If MSYS2 closes, start it again
   pacman -Su
   # Install required dependencies
   pacman -S mingw-w64-x86_64-toolchain git make libtool pkg-config autoconf automake texinfo mingw-w64-x86_64-libusb mingw-w64-x86_64-hidapi
   # download openocd package
   git clone https://github.com/raspberrypi/openocd.git --branch rp2040 --depth=1
   cd openocd
   # start to build openocd.exe
   ./bootstrap
   ./configure --disable-werror --enable-cmsis-dap
   make -j4
   ```

3. 測試`openocd.exe`是否正確安裝

   ```plaintext
   ./src/openocd.exe
   ```



### Linux setup

1. download openocd package

   ```
   git clone https://github.com/raspberrypi/openocd.git --branch rp2040 --depth=1
   ```

2. 複製檔案至相對應位置

   - openocd\
     - tool.sh
   - openocd\src\helper
     - replacement.h: 新增flash driver所需struct
   - openocd\src\flash\nor
     - advanced_elf_image.c: flash driver相關檔案
     - advanced_elf_image.h: flash driver相關檔案
     - FLASHPlugin.c: flash driver相關檔案
     - IMXRT1061_FLASH_WORKSIZE_8192_CMD_ERASE_CHIP_SFDP_TEST: flash driver
     - Makefile.am: 增加compile flash driver相關檔案
     - drivers.c: 增加plugin flash driver (flash bank command會呼叫使用)
   - openocd\tcl
     - imxrt_erase.cfg
   - openocd\openocd\tcl\target
     - imxrt.cfg
   - openocd\openocd\tcl\interface
     - cmsis-dap.cfg: 在linux環境下需要加入`adapter speed 5000`不然會有SWD clock的問題

3. 執行bootstrap下載相關library

   ```
   ./bootstrap
   ./configure --disable-werror --enable-cmsis-dap
   ```

4. 複製檔案至相對應位置

   - openocd\jimtcl
     - jim.c
     - jim.h

5. build openocd

   ```
   make -j4
   ```

   

### OpenOCD常用的Command

OpenOCD從Init開始，一路經過Adapter設定、TAP設定，直到最後Target設定~



OpenOCD常用的Command，主要分成以下幾種:

- Setup (Server & Debug Adapter Configuration)
  - 基本環境設定Command，讓OpenOCD能夠順利地完成初始化，直到整個Debug Adapter設定完成為止
  - **Server configuration**
    - `gdb_port  [number]`: 設定GDB連接去OpenOCD的Port number，預設號碼為3333
    - 針對多重target(multi-core、multi-target)，一個target就會配上一個GDB Server，也就是說每個target都會有一個對應的GDB Port
    - `telnet_port [number]`: 一個OpenOCD只會有一個Telnet Server，但不同於GDB一個Port僅能容許一個connetion，Telent可同時接受多個Connection
  - **Debug adapter configuration**
    - 設定啟動Debug Adapter的Driver: cmsis-dap
- TAP Declaration
  - Test Access Ports，為JTAG中的核心，而OpenOCD在連結Target的時候，必須要知道相關的設定才能夠正確的連接
  - 做為各個Target上的存取界面，包含JTAG串接的訊號介面、JTAG狀態機和相關指令/資料暫存器等等
  - `swj_newdap $_CHIPNAME cpu -irlen 4 -expected-id $_CPU_SWD_TAPID`
    - `-irlen [number]` : instruction register的bit數，通常為4 or 5 bits，看硬體怎麼設計
    - `-expected-id [numer]`: 用來讓OpenOCD判斷所連結的JTAG是否正確
- CPU Configuration
  - `target create [target_name] [type] [configparams]...`: 主要是用來定義TAP上的Target
    - target_name通常會用一個變數來指定
    - type，OpenOCD裡面支援多種Target，詳細所支援的Target可以參考[官方網站](http://openocd.org/doc/html/CPU-Configuration.html#CPU-Configuration)
    - `-endian` (big|little): 用來指定CPU屬於big endian或是little endian
  -  Target Event使用
    - OpenOCD在某些事件發生後，執行預先Hook好的那些指令
    - 以下是常用的Event:
      - debug-halted: 當Target進入Debug Mode時候，比方說踩到breakpoint時
      - debug-resumed: 當GDB將Target進入Resume時
      - gdb-attach: GDB連線時
      - gdb-detach: GDB斷線後
      - halted: Target進入halted後
      - reset-assert: 假如需要特別方式來做SRST時
      - **reset-deassert-post: SRST釋放後**
- Other Genernal Commands
  - Server相關的Genernal Commands
    - `debug_level [number]` : -3 ~ 3(more info)
    - `log_output [filename]`: 可以設定Log file的路徑，將預設會直接打印在OpenOCD的console上的Log，導向檔案中
  - Target State相關的Genernal Commands
    - `halt [ms]`: OpenOCD會等待[ms]或是預設的5秒
    - `resume [address]`: 讓Target在指定的位置做Resume，並從那個位置開始繼續執行下去。如果不指定Address的話，則從Halt當下的位置開始執行下去
    - `reset run`: 先Reset後再讓Target進入Free-Run的狀態
    - `reset halt`: 先Reset後再讓Target進入Halt的狀態
    - `reset` = `reset run`



### Openocd flash support introduction

一塊Flash裡面會切成一個或多個"Bank"

一個"Bank"中，裡面還會再切成多個"Sector"

每個Sector中會有自己的大小、Offset(跟Flash起始位置間的偏移)、狀態等等

Flash Driver是提供一系列的函式用來驅動(讀取/寫入/初始化)這個Bank

**src/flash/nor/core.h**

**Bank**

```c
struct flash_bank {
    const char *name;

    struct target *target; /**< Target to which this bank belongs. */

    struct flash_driver *driver; /**< Driver for this bank. */
    void *driver_priv; /**< Private driver storage pointer */

    int bank_number; /**< The 'bank' (or chip number) of this instance. */
    uint32_t base; /**< The base address of this bank */
    uint32_t size; /**< The size of this chip bank, in bytes */

    int chip_width; /**< Width of the chip in bytes (1,2,4 bytes) */
    int bus_width; /**< Maximum bus width, in bytes (1,2,4 bytes) */

    /** Erased value. Defaults to 0xFF. */
    uint8_t erased_value;

    /** Default padded value used, normally this matches the  flash
     * erased value. Defaults to 0xFF. */
    uint8_t default_padded_value;

    /**
     * The number of sectors on this chip.  This value will
     * be set intially to 0, and the flash driver must set this to
     * some non-zero value during "probe()" or "auto_probe()".
     */
    int num_sectors;
    /** Array of sectors, allocated and initialized by the flash driver */
    struct flash_sector *sectors;

    /**
     * The number of protection blocks in this bank. This value
     * is set intially to 0 and sectors are used as protection blocks.
     * Driver probe can set protection blocks array to work with
     * protection granularity different than sector size.
     */
    int num_prot_blocks;
    /** Array of protection blocks, allocated and initilized by the flash driver */
    struct flash_sector *prot_blocks;

    struct flash_bank *next; /**< The next flash bank on this chip */
};
```

- name: 幫這個Bank取名稱，方便之後用name找bank
- driver: 這邊放"Flash Driver"，操作都是從Bank找到Flash Driver後，再到底層去處理
- base/size: 就是放這個Bank的Base Address和大小
- num_sectors: 旗下Sectors的總數
- sectors: 每個Sector都有自己的狀態，這邊用一個Array存放指向這些Sector的指標



**Sector**

```c
struct flash_sector {
    /** Bus offset from start of the flash chip (in bytes). */
    uint32_t offset;
    /** Number of bytes in this flash sector. */
    uint32_t size;
    /**
     * Indication of erasure status: 0 = not erased, 1 = erased,
     * other = unknown.  Set by @c flash_driver_s::erase_check.
     *
     * Flag is not used in protection block
     */
    int is_erased;
    /**
     * Indication of protection status: 0 = unprotected/unlocked,
     * 1 = protected/locked, other = unknown.  Set by
     * @c flash_driver_s::protect_check.
     *
     * This information must be considered stale immediately.
     * A million things could make it stale: power cycle,
     * reset of target, code running on target, etc.
     *
     * If a flash_bank uses an extra array of protection blocks,
     * protection flag is not valid in sector array
     */
    int is_protected;
};
```

- offset: 跟Flash起始位置間的偏移量

- size: 這個Sector的大小 (注意: Sector大小可能不同，請參照文件)

- is_erased: Flash在寫入之前，要先進行Erase，這邊用來記錄這個Sector有沒有被清理過!

- is_protected: 用來保護這個Sector，防止被Erase/Program

  

### Openocd flash commands

- flash bank [name] [driver] [base addr] [size] [chip width] [bus width] [target] [driver options]

  - name: 給個方便使用的名稱
  - driver: 使用哪套Flash Driver來使用這塊Bank
  - base: Bank的Based Address
  - size: Bank的大小，可以不指定，但要補上0
  - chip_width: 用來標示這個Chip的頻寬，目前大部分的Driver都不需要指定
  - bus_width: 用來標示Data bus的頻寬，目前大部分的Driver都不需要指定
  - target: 用來標示這個Bank屬於哪個Target
  - driver_options: 如果Flash Driver需要其他參數，可以接在後面這邊

- flash write_image [erase] [unlock] [filename] [offset] [type]

  用來燒錄flash

  - erase: 表示在Program這個Flash之前，要先將Sector做Erase
  - unlock: 表示在Program這個Flash之前，要先將Sector做unlock
  - filename: Image的檔名
  - offset: 燒錄的偏移量
  - type: Image的檔案類型，OpenOCD支援以下幾種type
    - bin: Binary二進位檔案
    - elf: ELF檔案

- program [filename] [verify] [reset] [exit] [offset]

  燒錄code後執行指定動作

  - filename: 指定Image的檔案名稱
  - verify: 在燒錄完畢後，是否需要驗證燒錄的資料
  - reset: 燒錄完畢後，將Target進行Reset
  - exit: 燒錄完畢後，直接結束OpenOCD
  - offset: 燒錄的偏移量
  
  

### OpenOCD <-> GDB的操作

在GDB中要使用OpenOCD所支援的Command的時候，必須要使用以下方式: `monitor <Commands>`

- 可以使用`monitor help`看OpenOCD所支援的Commands
- 離開的方式，簡單地輸入`quit`即可
- 



## Experiment

### PC <-> Picoprobe <-> LPC824 (45MR-1600)

**lpc8xx.cfg**

- WORKAREASIZE: 設置flash燒錄過程中可以使用的RAM size
- call lpc1xxx.cfg



**lpc1xxx.cfg**

- CHIPNAME = lpc8xx

- CHIPSERIES = lpc800

- CCLK (CPU core clock freq) = 12Mhz (default)

  - 45MR core freq為30Mhz，實驗結果一樣

  

**SW flash program with default setting (fail)**

```plaintext
cd openocd
./src/openocd.exe -f interface/cmsis-dap.cfg -f target/lpc8xx.cfg -s tcl -c "program C:/Users/MeichiMC_Wang/Desktop/red_led.bin exit 0x00000000"
```



**使用openod default target config檔輸出結果**

- Warn: boot過程中MCU會計算向量表的checksum與image裡的checksum做比對，兩者計算結果不同，但是openocd會寫入正確的值
  - 拿掉checksum功能
- Error: 沒有足夠的working area (RAM)做燒錄動作 (default為1KB)
  - 加大RAM size至2KB

```plaintext
Open On-Chip Debugger 0.11.0-g8e3c38f (2023-03-16-08:57)
Licensed under GNU GPL v2
For bug reports, read
        http://openocd.org/doc/doxygen/bugs.html
adapter speed: 5000 kHz

Info : auto-selecting first available session transport "swd". To override use 'transport select <transport>'.
cortex_m reset_config sysresetreq

Info : Using CMSIS-DAPv2 interface with VID:PID=0x2e8a:0x000c, serial=E660B440076CB226
Info : CMSIS-DAP: SWD  Supported
Info : CMSIS-DAP: FW Version = 2.0.0
Info : CMSIS-DAP: Interface Initialised (SWD)
Info : SWCLK/TCK = 0 SWDIO/TMS = 0 TDI = 0 TDO = 0 nTRST = 0 nRESET = 0
Info : CMSIS-DAP: Interface ready
Info : clock speed 10 kHz
Info : SWD DPIDR 0x0bc11477
Info : lpc8xx.cpu: hardware has 4 breakpoints, 2 watchpoints
Info : starting gdb server for lpc8xx.cpu on 3333
Info : Listening on port 3333 for gdb connections
target halted due to debug-request, current mode: Thread
xPSR: 0xf1000000 pc: 0x1fff0008 msp: 0x10000ffc
** Programming Started **
Warn : Boot verification checksum in image (0x00000000) to be written to flash is different from calculated vector checksum (0xefffe63d).
Warn : OpenOCD will write the correct checksum. To remove this warning modify build tools on developer PC to inject correct LPC vector checksum.
Warn : not enough working area available(requested 1024)
Error: no working area specified, can't write LPC2000 internal flash
Error: error writing to flash at address 0x00000000 at offset 0x00000000
** Programming Failed **
shutdown command invoked
```



**SW flash program (fail -> success)**

```plaintext
./src/openocd.exe -f interface/cmsis-dap.cfg -f target/lpc8xx.cfg -s tcl -c "program C:/Users/MeichiMC_Wang/Desktop/red_led.bin verify reset exit 0x00000000" -c "adapter speed 5000"
```



**使用更改過的openod default target config檔輸出結果**

- Warn: boot過程中MCU會計算向量表的checksum與image裡的checksum做比對，兩者計算結果不同，但是openocd會寫入正確的值
  - 拿掉checksum功能 -> :exclamation:**checksum功能不可省略**
- Error: 沒有足夠的working area (RAM)做燒錄動作 (default為1KB)
  - 加大RAM size至2KB
- 45MR led沒亮

燒錄過程中沒有比對checksum功能output (fail):

```plaintext
Open On-Chip Debugger 0.11.0-g8e3c38f (2023-03-16-08:57)
Licensed under GNU GPL v2
For bug reports, read
        http://openocd.org/doc/doxygen/bugs.html
adapter speed: 5000 kHz

Info : auto-selecting first available session transport "swd". To override use 'transport select <transport>'.
cortex_m reset_config sysresetreq

Info : Using CMSIS-DAPv2 interface with VID:PID=0x2e8a:0x000c, serial=E660B440076CB226
Info : CMSIS-DAP: SWD  Supported
Info : CMSIS-DAP: FW Version = 2.0.0
Info : CMSIS-DAP: Interface Initialised (SWD)
Info : SWCLK/TCK = 0 SWDIO/TMS = 0 TDI = 0 TDO = 0 nTRST = 0 nRESET = 0
Info : CMSIS-DAP: Interface ready
Info : clock speed 10 kHz
Info : SWD DPIDR 0x0bc11477
Info : lpc8xx.cpu: hardware has 4 breakpoints, 2 watchpoints
Info : starting gdb server for lpc8xx.cpu on 3333
Info : Listening on port 3333 for gdb connections
target halted due to debug-request, current mode: Thread
xPSR: 0xf1000000 pc: 0x1fff0008 msp: 0x10000ffc
** Programming Started **
** Programming Finished **
** Verify Started **
** Verified OK **
** Resetting Target **
shutdown command invoked
```



加大RAM設置+燒錄過程比對checksum(success):

```plaintext
** Programming Started **
Warn : Boot verification checksum in image (0x00000000) to be written to flash is different from calculated vector
checksum (0xefffe63d).
Warn : OpenOCD will write the correct checksum. To remove this warning modify build tools on developer PC to inject
 correct LPC vector checksum.
** Programming Finished **
** Verify Started **
Error: checksum mismatch - attempting binary compare
** Verify Failed **
shutdown command invoked
```



### PC <-> DAPLink <-> IMXRT1061

program bootstrap:

```
./src/openocd.exe -f interface/cmsis-dap.cfg -f target/imxrt.cfg -c "adapter speed 3000" -s tcl -c "program C:/Users/MeichiMC_Wang/Desktop/bootstrap.bin exit 0x60000000" -c init
```

program bootloader:

```
./src/openocd.exe -f interface/cmsis-dap.cfg -f target/imxrt.cfg -s tcl -c "program C:/Users/MeichiMC_Wang/Desktop/bootloader_V1.0.0.0_Build_23032108.bin exit 0x60020000" -c init
```

erase the chip (0x600000000~0x607fffff):

```
./src/openocd.exe -f interface/cmsis-dap.cfg -f target/imxrt.cfg -s tcl -c "flash init; init; reset halt; flash erase_sector 0 0 last; exit"
```



## Appendix A (JESD216)

JESD216定義了Serial Flash Discoverable Parameters (SFDP)用於NOR flash和NAND flash，描述flash參數與規格的表格。

### How to access SFDP

- `0x5A`: 將SFDF表由flash讀取至主機
- `0x5B`: 將SFDF表由flash copy到 flash緩衝區，適用於Non-Volatile Memory

Serial NOR Flash設備，僅使用`0x5A`即可讀取SFDP表至主機。Flash driver可能不知道flash為SPI或QSPI等模式，因此按照JESD216定義driver應當先使用QSPI探測SFDP是否存在，如果失敗再使用SPI模式。

QSPI 4-4-4 (command, address和data的bus寬度皆為4) -> DSPI -> SPI

注意:

雖然JESD216有定義主機向flash傳送`0x5A`格式應為 1byte命令 + 3 byte地址 + 8個時鐘周期dummy + 數據，但不是所有flash都支援8個時鐘周期dummy，因此flash driver再特測階段會發送不同個數周期的dummy

### SFDP format

SFDP由標題和參數表構成，標題用來描述參數表的版本、類型、大小等，而參數表紀錄具體的參數值，總共為9 DWORDs。

### SFDP header structure

![img](images/SFDP_header_struct.jpg)

![img](images/SFDP_header_example.jpg)

### JEDEC 參數內容

- 支援1(cmd)-1(addr)-4(data) / 1-4-4 / 1-2-2 / 1-1-2 / 4-4-4 / 2-2-2 fast read (0/1)
- 支援double transfer rate clocking
- address bytes (3 default/4 bytes)
- Block/Sector Erase Sizes (01: 4KB erase)
- flash大小: 如果第31個bit為0 (<2GB)，則後面的數值轉為十進制即為flash size，如果第31個bit為1 (>4GB)，則後面數值轉換為十進制後2^N
- 不同傳輸模式所需的dummy clock個數
- 紀錄Opcode

*DWORD = 32 bits

*number of parameter headers = 0x00 表示1個

### Read SFDP behavior

- SFDP與flash memory address是不會重疊的，且在製造商出廠後不得被更改
- 如果flash有支援reset和hold功能，可以在下讀取SFDP指令過程中被執行
- 未使用到的SFDP空間建議填入0xFF



## Reference

1. [Day 27: 高手不輕易透露的技巧(1/2) - Flash Programming](https://ithelp.ithome.com.tw/articles/10197190)
2. [Day 28: 高手不輕易透露的技巧(2/2) - Flash Driver & Target Burner](https://ithelp.ithome.com.tw/articles/10197309?sc=iThelpR)



# IMXRT1xxx

## RT1060 spec

### vector table address offset

![img](images/imxrt_cfg-1.jpg)

## 如何在flexspi nor flash模式下調試

### XIP啟動流程

BootRom根據BOOT_MODE和eFuse狀態決定使用哪一個啟動設備，使用 eFUSEs決定啟動設備可能會被 GPIO 輸入引腳所覆蓋。

![img](images/XIP%E5%95%9F%E5%8B%95%E8%A8%AD%E5%82%99SW.jpg)

![img](images/flexspi_nor%E5%95%9F%E5%8B%95%E6%B5%81%E7%A8%8B.jpg)



FlexSPI NOR Flash可執行文件由以下構成:

- flash配置參數: 讀取命令序列、flexspi頻率、quad模式
- IVT: 讓ROM知道可執行檔個component位址
- 啟動數據: 用來指定可執行檔的位置 (boot_data)
- DCD: IC配置數據 (例如: SDRAM 配置，refer to SDK中的dcd_data

![img](images/destination_memory.jpg)



**MIMXRT1052xxxxx_flexspi_nor.icf**可以看到配置參數、IVT、啟動數據、DCD數據在flash中的布局。

![img](images/%E5%95%9F%E5%8B%95%E6%96%87%E4%BB%B6%E4%BD%8D%E7%BD%AE%E8%A8%8A%E6%81%AF.jpg)



## 藉由visual studio + visualGDB 開發imxrt flash driver

參考文章: [Programming the FLASH memory of i.MXRT devices with OpenOCD](https://visualgdb.com/tutorials/arm/imxrt/flash/)

**openocd plugin flash driver interface -> IMXRT1061_FLASH drive**r

1. visualGDB將MCUXpresso SDK載入，引用sample code中romapi

2. 在project中加入 [common FLASH plugin sources](https://github.com/sysprogs/flash_drivers/tree/master/common) FLASHPluginCommon.cpp和FLASHPluginInterface.h (openocd flash driver interface，plugin藉由FLASHPluginInterface.h call functions)

3. 改寫romapi code，組合成符合openocd flash driver interface的功能

   ```c
   int FLASHPlugin_DoProgramSync(unsigned startOffset, const void *pData, int bytesToWrite)
   {
   	int pages = bytesToWrite / FLASH_PAGE_SIZE;
   	for (int i = 0; i < pages; i++)
   	{
   		status_t status = ROM_FLEXSPI_NorFlash_ProgramPage(FlexSpiInstance,
   			&norConfig,
   			startOffset + i * FLASH_PAGE_SIZE,
   			(const uint32_t *)((const char *)pData + i  * FLASH_PAGE_SIZE));
   		
   		if (status != kStatus_Success)
   			return i * FLASH_PAGE_SIZE;
   	}
   	
   	return pages * FLASH_PAGE_SIZE;
   }
   ```

   

4. 加入FLASHPluginConfig.h 其中定義 spi flash 的資訊，page size等等

5. build IMRT1061_FLASH 產生elf檔

6. 建立sample code blink_led，選用`IMXRT1061xxxxx_flexspi_nor.ld`，設定debug command

   ```plaintext
   -f interface/cmsis-dap.cfg -c "adapter speed 3000" -f target/imxrt.cfg -c "flash bank imxrt plugin $FLASH_MEMORY_BASE 0 0 0 0 $(ProjectDir.forwardslashes)/IMXRT1061_FLASH" -c init
   ```

   ![img](images/blinkled_visualgdb_debug_setting.jpg)

   

   Output (success):

   注意: 其燒錄位置由0x60002000 app address開始，此作法是先將0x60000000-0x60002000先藉由MCUXpresso romapi sample code做寫入

   ```plaintext
   Open On-Chip Debugger 0.12.0 (2023-02-02) [https://github.com/sysprogs/openocd]
   Licensed under GNU GPL v2
   libusb1 09e75e98b4d9ea7909e8837b7a3f00dda4589dc3
   For bug reports, read
   	http://openocd.org/doc/doxygen/bugs.html
   adapter speed: 3000 kHz
   
   Info : auto-selecting first available session transport "swd". To override use 'transport select <transport>'.
   none separate
   
   Warn : use 'imxrt.cpu' as target identifier, not '0'
   Info : CMSIS-DAP: SWD supported
   Info : CMSIS-DAP: Atomic commands supported
   Info : CMSIS-DAP: Test domain timer supported
   Info : CMSIS-DAP: FW Version = 2.1.0
   Info : CMSIS-DAP: Serial# = 070000011a5a5bb50000000001d36091a5a5a5a597969908
   Info : CMSIS-DAP: Interface Initialised (SWD)
   Info : SWCLK/TCK = 1 SWDIO/TMS = 1 TDI = 0 TDO = 0 nTRST = 0 nRESET = 1
   Info : CMSIS-DAP: Interface ready
   Info : clock speed 3000 kHz
   Info : SWD DPIDR 0x0bd11477
   Info : [imxrt.cpu] Cortex-M7 r1p1 processor detected
   Info : [imxrt.cpu] target has 8 breakpoints, 4 watchpoints
   Info : starting gdb server for imxrt.cpu on 50738
   Info : Listening on port 50738 for gdb connections
   VisualGDB_OpenOCD_Ready
   Info : Listening on port 6666 for tcl connections
   Info : Listening on port 50736 for telnet connections
   Info : accepting 'gdb' connection on tcp/50738
   Warn : keep_alive() was not invoked in the 1000 ms timelimit. GDB alive packet not sent! (1828 ms). Workaround: increase "set remotetimeout" in GDB
   Warn : Prefer GDB command "target extended-remote :50738" instead of "target remote :50738"
   Warn : keep_alive() was not invoked in the 1000 ms timelimit. GDB alive packet not sent! (1281 ms). Workaround: increase "set remotetimeout" in GDB
   Error executing event gdb-flash-erase-start on target imxrt.cpu:
   wrong # args: should be "expr expression"
   Info : Erasing FLASH: 0x60002000-0x60006000...
   Warn : keep_alive() was not invoked in the 1000 ms timelimit. GDB alive packet not sent! (1395 ms). Workaround: increase "set remotetimeout" in GDB
   Info : Programming FLASH (1 sections, 15252 bytes)...
   Info : Programming FLASH section 1/1 (15252 bytes) at 0x60002000...
   Info : flash_write_progress_sync:0x60002000|0x60003000|imxrt
   Info : flash_write_progress_sync:0x60003000|0x60004000|imxrt
   Info : flash_write_progress_sync:0x60004000|0x60005000|imxrt
   Info : flash_write_progress_sync:0x60005000|0x60005b00|imxrt
   Info : flash_write_progress_sync:0x60005b00|0x60005c00|imxrt
   Warn : keep_alive() was not invoked in the 1000 ms timelimit. GDB alive packet not sent! (1871 ms). Workaround: increase "set remotetimeout" in GDB
   Error executing event gdb-flash-write-end on target imxrt.cpu:
   wrong # args: should be "expr expression"
   Info : dropped 'gdb' connection (error -400)
   shutdown command invoked
   Warn : Flash driver of imxrt does not support free_driver_priv()
   ```

   
